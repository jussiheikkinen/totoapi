var express    = require('express');
var bodyParser = require('body-parser');
var mongoose   = require('mongoose');
var auth = require('basic-auth');
var passport = require('passport');
var session = require('express-session');
var cookieParser = require('cookie-parser');

var routes = require('./routes/heppa');
var kanta = require('./routes/kanta');
var tilastot = require('./routes/tilastot');
var lisaa = require('./routes/lisaaDataa');
var mongoDump = require('./routes/export');
var viesti = require('./routes/viestit');
var defaultRoute = require('./routes/default');
var Requestlog = require('./models/requestlog');
var Kayttaja = require('./models/kayttaja');

var app = express();
var port = process.env.PORT || 8080;
var router = express.Router();

mongoose.connect('mongodb://localhost:27017/TotoKanta');

app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.static(__dirname + '/public'));
app.use(bodyParser.json());
app.use(cookieParser());

app.use(session({ secret: 'ilovetoto', resave: true, saveUninitialized: true })); // session secret
app.use(passport.initialize());
app.use(passport.session()); // persistent login sessions
require('./passport')(passport);

router.use(function(req, res, next) {
    //middleware loggaa käynnit
    var now = new Date();
    var requestlog = new Requestlog();
    requestlog.ip = "000.000.000";
    requestlog.aika = now;

    requestlog.save(function(err) {
        if (err) res.send(err);
    });
    console.log('New request to server');
    next();
});

routes(router);
kanta(router);
lisaa(router);
tilastot(router);
viesti(router);
mongoDump(router);
defaultRoute(router);
//prefix esim localhost/api/*
app.use('/toto', router);
app.listen(port);
console.log('Api connected: ' + port);
