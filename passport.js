var LocalStrategy   = require('passport-local').Strategy;
var User = require('./models/kayttaja');

module.exports = function(passport) {

    // used to serialize the user for the session
    passport.serializeUser(function(user, done) {
        done(null, user.id);
    });

    // used to deserialize the user
    passport.deserializeUser(function(id, done) {
        User.findById(id, function(err, user) {
            done(err, user);
        });
    });

    passport.use('local', new LocalStrategy({
            usernameField : 'username',
            passwordField : 'password',
            passReqToCallback : true
        },
        function(req, username, password, done) {

            User.findOne({ 'local.username' :  username }, function(err, user) {
                // Käyttäjän validointi
                if (err){
                  console.log("error");
                  return done(err);
                  }
                  console.log(user);

                if (!user){
                console.log("ei käyttäjää");
                    return done(null, false);
                  }

                if (!user.validPassword(password)){
                console.log("väärä salasana");
                    return done(null, false);
                  }

                console.log("Käyttäjä tunnistettu");
                return done(null, user);
            });

        }));

};
