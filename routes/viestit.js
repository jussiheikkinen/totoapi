'use strict';

var mongoose = require('mongoose');
var Viesti = require('../models/viesti');
var express = require('express');
var bodyParser = require('body-parser');

module.exports = (function(router){

router.get('/viestit', function(req, res,next) {
Viesti.find({}).then(function(viestit){
  var arr = [];
  for (var i = 0; i<viestit.length; i++){
    arr.push({"nimi": viestit[i].nimi, "viesti": viestit[i].comment, "aika": viestit[i].aika});
  }
  res.json(arr);
}, function(err){
  res.json({"error": "ei hakutuloksia"});
  });
});


router.post('/viestit/uusi', function(req, res) {
    var viesti = new Viesti();
    var aika = new Date();
    viesti.aika = aika;
    viesti.nimi = req.body.nimi;
    viesti.comment = req.body.viesti;
    console.log(req.body.nimi + "  " + req.body.viesti);

    viesti.save(function(err) {
        if (err) res.json({"virhe": "ei tiivä mikä"});
        console.log('Viesti tallennettu');
        res.json({"Ok": "viesti tallennettu"}, 200);
    });
});

return router;
});
