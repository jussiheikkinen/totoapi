'use strict';

var mongoose = require('mongoose');
var Hevonen = require('../models/hevonen');
var Sukupuoli = require('../models/sukupuoli');
var Vihje = require('../models/vihje');
var express = require('express');
var fs = require('fs');
var path = require('path');

module.exports = (function(router){

router.get('/tilastot/voitot', function(req, res) {
  Hevonen.find({'ura.voitot': {$gt: 5}}, function (err, result){
    if (err){
      res.json(err);
    }else{
      res.json(result);
    }
  });
});

router.get('/tilastot/kisat/ohjaaja/:id', function(req, res) {
  Hevonen.find({"lahdot.valmentaja": req.params.id}, function (err, result){
    if (err){
      res.json(err);
    }else{
      res.json(result);
    }
  });
});

router.get('/tilastot/vihje', function(req, res,next) {
    Vihje.find({}).then(function(vihjeet){
        res.json(vihjeet);
      }, function(err){
          res.json({"error": "tuntematon virhe"});
      });
});

router.post('/tilastot/hevonen/:id', function(req, res,next) {
Hevonen.findOne({"hevonenid": req.params.id}).then(function(heppa){
  var voitot = heppa.voitot;
  var kakkoset = heppa.kakkoset;
  var kolmoset = heppa.kolmoset;
  res.json({"kilpailuja": heppa.startit, "sijoituksia": voitot + kakkoset + kolmoset});
  });
});

router.get('/tilastot/sukupuoli/:sukupuoli', function(req, res,next) {
Hevonen.find({"sukupuoli": req.params.sukupuoli}).then(function(sukupuoli){

  var voitot = 0, kakkoset = 0, kolmoset = 0, startit = 0;
  for(var i = 0;i<sukupuoli.length;i++){
    voitot    += sukupuoli[i].ura.voitot;
    kakkoset  += sukupuoli[i].ura.kakkoset;
    kolmoset  += sukupuoli[i].ura.kolmoset;
    startit   += sukupuoli[i].ura.startit;
  }
    var objekti = new Sukupuoli();
    objekti.sukupuoli = req.params.sukupuoli;
    objekti.startit = startit;
    objekti.voitot = voitot
    objekti.kakkoset = kakkoset;
    objekti.kolmoset = kolmoset;

    objekti.save({}).then(function(){
    res.json({"Laji": req.params.sukupuoli, "kilpailuja": startit, "voitot": voitot, "kakkoset": kakkoset, "kolmoset": kolmoset});
    });
  }, function(err){
    res.json({"Virhe": "tuntematon laji"});
  });
});
/*
//Multiparametrinen haku mongokannasta jolla haetaan vain spesifit objektiti tauluista
router.get('/tilastot/4/:param1/:param2/:param3/:param4', function(req, res) {
  var p1 =req.params.param1;
  var p2 =req.params.param2;
  var p3 =req.params.param3 + ".pvm";
  var p4 =req.params.param4;
  //-------------mikä----mitä---taulukosta-------------mistä-----mitä
  Hevonen.find({p1: p2},{p3: p4}, function (err, result){
    if (err){
      res.json(err);
    }else{
      res.json(result);
    }
  });
});
*/
return router;
});
