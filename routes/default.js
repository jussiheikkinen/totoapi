//kaikki frontendin sivut ohjautu tänne
'use strict';
var express = require('express');
var Kayttaja = require('../models/kayttaja');
var passport = require('passport');
var path = require('path');
var loggedin = require("../helpers/loggedin");

module.exports = (function(router){


router.get('/uusi/:username/:password', function(req, res, next) {
    var kayttaja = new Kayttaja();
    kayttaja.local.username = req.params.username;
    kayttaja.local.password = req.params.password;

    kayttaja.save(function(err) {
        if (err) res.send(err);
        console.log('Uusi kayttaja luotu' + kayttaja);
      });
    });

router.get('/login', function(req, res) {
        res.sendFile(path.join(__dirname, '../public/views/login.html'));
    });

router.post('/login', passport.authenticate('local', {
        successRedirect : '/toto',
        failureRedirect : '/toto/login'
    }));

router.get('/etusivu', function(req, res) {
          res.sendFile(path.join(__dirname, '../public/views/landingpage.html'));
    });

router.get('*', loggedin.isLoggedIn, function(req, res) {
        res.sendFile(path.join(__dirname, '../public/views/index.html'));
    });

return router;
});
