'use strict';

var mongoose = require('mongoose');
var Hevonen = require('../models/hevonen');
var Vihje = require('../models/vihje');
var Tallikommentti = require('../models/tallikommentti');
var express = require('express');

module.exports = (function(router){

  router.post('/lisaa/vihje', function(req, res,next) {
    var vihje = new Vihje();
    var aika = new Date();
    vihje.aika = aika;
    vihje.kilpailu = req.body.kilpailu;
    vihje.vihje= req.body.vihje;

      vihje.save({}).then(function(){
          res.json({"msg": "vihje tallennettu"});
        }, function(err){
            res.json({"error": "vihjetta ei lisatty"});
        });
  });

  router.post('/lisaa/tallikommentti', function(req, res,next) {
    var tallikommentti = new Tallikommentti();
    var aika = new Date();
    tallikommentti.aika = aika;
    tallikommentti.talli = req.body.talli;
    tallikommentti.kommentti = req.params.kommentti;

      tallikommentti.save({}).then(function(){
          res.json({"msg": "kommentti tallennettu"});
        }, function(err){
          res.json({"error": "Kommenttia ei lisatty"});
        });
  });


return router;
});
