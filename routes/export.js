'use strict';

var mongoose = require('mongoose');
var Hevonen = require('../models/hevonen');
var Kayttaja = require('../models/kayttaja');
var express = require('express');
var fs = require('fs');
var path = require('path');

module.exports = (function(router){

  router.route('/export/hepat').get(function(req, res) {
    var date = new Date();
    var paiva = date.getDay();
    var kuukausi = date.getMonth();
    var vuosi = date.getFullYear();
  Hevonen.find(function(err, hevoset) {
          if (err) res.send(err);
          fs.writeFile(path.join(__dirname,'../backup/mongo_dump_hepat'+ paiva + kuukausi +vuosi +'.txt'), hevoset,  function(err) {
              if (err) {
                res.json({"virhe": err}, 400);
              }else{
              console.log("Data written successfully!");
              res.json({"Tietokanta varmuuskopioitu": date}, 200);
            }
        });
      });
    });


    router.route('/export/kayttajat').get(function(req, res) {
      var date = new Date();
      var paiva = date.getDay();
      var kuukausi = date.getMonth();
      var vuosi = date.getFullYear();
    Kayttaja.find(function(err, kayttajat) {
            if (err) res.send(err);
            fs.writeFile(path.join(__dirname,'../backup/mongo_dump_kayttajat'+ paiva + kuukausi +vuosi +'.txt'), kayttajat,  function(err) {
                if (err) {
                  res.json({"virhe": err}, 400);
                }else{
                console.log("Data written successfully!");
                res.json({"Tietokanta varmuuskopioitu": date}, 200);
              }
          });
        });
      });

return router;
});
