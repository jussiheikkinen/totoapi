'use strict';

var mongoose = require('mongoose');
var Hevonen = require('../models/hevonen');
var Lahto = require('../models/lahto');
var express = require('express');
var fs = require('fs');
var request = require('request');
var cheerio = require('cheerio');


module.exports = (function(router){

  router.route('/kanta/laske').get(function(req, res) {

    Hevonen.count({}, function( err, count){
    console.log( "Hevosia kannassa:", count );
    res.json({"Hevosia kannassa": count});
  })

});


  router.route('/kanta/parsi/:id').get(function(req, res) {

      var url = 'http://ravit2.iltasanomat.fi/hevoset/'+ req.params.id;

      request(url, function(error, response, html){

              if(!error){
                  //utilize the cheerio library
                  var $ = cheerio.load(html);
                  // Finally, we'll define the variables we're going to capture
                  var nimi, sukupuoli, laji, ika, Valmentaja, omistaja;
                  var startit, kakkoset, voitot, kolmoset, voittosumma;
                  var ryhmalahto, tasoitusajo;
                  var pvm, lahto, ratanro, matka, sijoitus, palkkio, valmentaja, ohjaaja, kmaika, kerroin;
                  var heppa = new Hevonen();

                    $('.hevostilastot').eq(1).filter(function(){

                    var data = $(this);

                    nimi = data.children().children().children().eq(1).text();
                    laji = data.children().children().eq(1).children().eq(1).text();
                    sukupuoli = data.children().children().eq(2).children().eq(1).text();
                    //ikä puuttuu usein jolloin valmentaja on 3 ja omistaja 4
                    var check = data.children().children().eq(3).children().eq(0).text();
                    console.log(check + check.length);
                    if (check === 'VALMENTAJA: '){
                      ika = 0;
                      valmentaja = data.children().children().eq(3).children().eq(1).text();
                      omistaja = data.children().children().eq(4).children().eq(1).text();

                      startit = data.children().children().eq(7).children().eq(1).text();
                      voitot = data.children().children().eq(8).children().eq(1).text();
                      kakkoset = data.children().children().eq(9).children().eq(1).text();
                      kolmoset = data.children().children().eq(10).children().eq(1).text();
                      voittosumma = data.children().children().eq(11).children().eq(1).text();
                      ryhmalahto = data.children().children().eq(20).children().eq(1).text();
                      ryhmalahto = ryhmalahto.substring(0,2) + "." + ryhmalahto.substring(3,4);
                      tasoitusajo = data.children().children().eq(21).children().eq(1).text();
                      tasoitusajo = tasoitusajo.substring(0,2) + "." + tasoitusajo.substring(3,4);
                    }else{
                    ika = data.children().children().eq(3).children().eq(1).text();
                    valmentaja = data.children().children().eq(4).children().eq(1).text();
                    omistaja = data.children().children().eq(5).children().eq(1).text();

                    startit = data.children().children().eq(8).children().eq(1).text();
                    voitot = data.children().children().eq(9).children().eq(1).text();
                    kakkoset = data.children().children().eq(10).children().eq(1).text();
                    kolmoset = data.children().children().eq(11).children().eq(1).text();
                    voittosumma = data.children().children().eq(12).children().eq(1).text();
                    ryhmalahto = ryhmalahto.substring(0,2) + "." + ryhmalahto.substring(3,4);
                    tasoitusajo = data.children().children().eq(22).children().eq(1).text();
                    tasoitusajo = tasoitusajo.substring(0,2) + "." + tasoitusajo.substring(3,4);
                  }

                    heppa.hevonenid = req.params.id;
                    heppa.nimi = nimi;
                    heppa.laji = laji;
                    heppa.sukupuoli = sukupuoli;
                    heppa.ika = parseInt(ika);
                    heppa.valmentaja = valmentaja;
                    heppa.omistaja = omistaja;

                    heppa.ura.startit = !isNaN(startit) && startit ? parseInt(startit):0;
                    heppa.ura.voitot = !isNaN(voitot) && voitot ? parseInt(voitot):0;
                    heppa.ura.kakkoset = !isNaN(kakkoset) && kakkoset ? parseInt(kakkoset):0;
                    heppa.ura.kolmoset = !isNaN(kolmoset) && kolmoset ? parseInt(kolmoset):0;
                    heppa.ura.voittosumma = !isNaN(voittosumma) && voittosumma ? parseInt(voittosumma):0;
                    heppa.ennatysajat.ryhmalahto = !isNaN(ryhmalahto) && ryhmalahto  ? parseFloat(ryhmalahto):0;
                    heppa.ennatysajat.tasoitusajo = !isNaN(tasoitusajo) && tasoitusajo ? parseFloat(tasoitusajo): 0;

                    for (var i = 2; i<22;i++){

                      var lahto = new Lahto();

                      var rata = data.children().eq(3).children().children().eq(i).children().eq(0).text();
                      lahto.rata = rata;
                      lahto.pvm = data.children().eq(3).children().children().eq(i).children().eq(1).text();
                      lahto.lahto = parseInt(data.children().eq(3).children().children().eq(i).children().eq(2).text());
                      lahto.ratanro = parseInt(data.children().eq(3).children().children().eq(i).children().eq(3).text());
                      lahto.matka = parseInt(data.children().eq(3).children().children().eq(i).children().eq(4).text());
                      lahto.sijoitus = parseInt(data.children().eq(3).children().children().eq(i).children().eq(5).text());
                      lahto.kmaika = parseFloat(data.children().eq(3).children().children().eq(i).children().eq(7).text());
                      lahto.kerroin = parseFloat(data.children().eq(3).children().children().eq(i).children().eq(9).text());
                      lahto.palkkio = parseInt(data.children().eq(3).children().children().eq(i).children().eq(10).text());
                      lahto.valmentaja = data.children().eq(3).children().children().eq(i).children().eq(11).text();
                      lahto.ohjaaja = data.children().eq(3).children().children().eq(i).children().eq(12).text();

                      if (rata.length != 0){
                        heppa.lahdot.push(lahto);
                      }
                  }

              })
              //Tallennetaan heppa mongoon
              heppa.save(function(err) {
                  if (err)
                      res.send(err);
                  console.log('Hevonen created!');
              });

            res.json(heppa);
          };
        })
      });




router.route('/kanta/parsi/:alku/:loppu').get(function(req, res) {
          //res.setTimeout(3 * 60 * 1000);// 120000ms kunnes katkaisee yhteyden
          var luku = req.params.loppu - req.params.alku;
          var events = require('events');
          var parseid = 1;
        laskeHepat(req.params.alku, req.params.loppu, luku);

        function laskeHepat(alku, loppu, luku){
          console.log( "params:", alku +" "+ loppu +" "+ luku);
          Hevonen.count({}, function( err, count){
            console.log( "Hevosia kannassa:", count );

          var heppaid = count - 1;

          (function loop(){
            if (alku <= loppu){

          request('http://ravit2.iltasanomat.fi/hevoset/'+ alku, function(error, response, html){

                  if(!error){

                      var $ = cheerio.load(html);

                      var nimi, sukupuoli, laji, ika, valmentaja, omistaja;
                      var startit, kakkoset, voitot, kolmoset, voittosumma;
                      var ryhmalahto, tasoitusajo;
                      var heppa = new Hevonen();

                        $('.hevostilastot').eq(1).filter(function(){

                        var data = $(this);

                        nimi = data.children().children().children().eq(1).text();
                        laji = data.children().children().eq(1).children().eq(1).text();
                        sukupuoli = data.children().children().eq(2).children().eq(1).text();
                        //ikä puuttuu usein jolloin valmentaja on 3 ja omistaja 4
                        var check = data.children().children().eq(3).children().eq(0).text();

                        if (check === 'VALMENTAJA: '){
                          ika = 0;
                          valmentaja = data.children().children().eq(3).children().eq(1).text();
                          omistaja = data.children().children().eq(4).children().eq(1).text();

                          startit = data.children().children().eq(7).children().eq(1).text();
                          voitot = data.children().children().eq(8).children().eq(1).text();
                          kakkoset = data.children().children().eq(9).children().eq(1).text();
                          kolmoset = data.children().children().eq(10).children().eq(1).text();
                          voittosumma = data.children().children().eq(11).children().eq(1).text();
                          ryhmalahto = data.children().children().eq(20).children().eq(1).text();
                          ryhmalahto = ryhmalahto.substring(0,2) + "." + ryhmalahto.substring(3,4);
                          tasoitusajo = data.children().children().eq(21).children().eq(1).text();
                          tasoitusajo = tasoitusajo.substring(0,2) + "." + tasoitusajo.substring(3,4);
                        }else{
                        ika = data.children().children().eq(3).children().eq(1).text();
                        valmentaja = data.children().children().eq(4).children().eq(1).text();
                        omistaja = data.children().children().eq(5).children().eq(1).text();

                        startit = data.children().children().eq(8).children().eq(1).text();
                        voitot = data.children().children().eq(9).children().eq(1).text();
                        kakkoset = data.children().children().eq(10).children().eq(1).text();
                        kolmoset = data.children().children().eq(11).children().eq(1).text();
                        voittosumma = data.children().children().eq(12).children().eq(1).text();
                        ryhmalahto = data.children().children().eq(21).children().eq(1).text();
                        ryhmalahto = ryhmalahto.substring(0,2) + "." + ryhmalahto.substring(3,4);
                        tasoitusajo = data.children().children().eq(22).children().eq(1).text();
                        tasoitusajo = tasoitusajo.substring(0,2) + "." + tasoitusajo.substring(3,4);
                      }

                        heppa.hevonenid = heppaid;
                        heppa.nimi = nimi;
                        heppa.laji = laji;
                        heppa.sukupuoli = sukupuoli;
                        heppa.ika = parseInt(ika);
                        heppa.valmentaja = valmentaja;
                        heppa.omistaja = omistaja;

                        heppa.ura.startit = !isNaN(startit) && startit ? parseInt(startit):0;
                        heppa.ura.voitot = !isNaN(voitot) && voitot ? parseInt(voitot):0;
                        heppa.ura.kakkoset = !isNaN(kakkoset) && kakkoset ? parseInt(kakkoset):0;
                        heppa.ura.kolmoset = !isNaN(kolmoset) && kolmoset ? parseInt(kolmoset):0;
                        heppa.ura.voittosumma = !isNaN(voittosumma) && voittosumma ? parseInt(voittosumma):0;
                        heppa.ennatysajat.ryhmalahto = !isNaN(ryhmalahto) && ryhmalahto  ? parseFloat(ryhmalahto):0;
                        heppa.ennatysajat.tasoitusajo = !isNaN(tasoitusajo) && tasoitusajo ? parseFloat(tasoitusajo): 0;

                        for (var i = 2; i<22;i++){

                        var lahto = new Lahto();

                        var rata = data.children().eq(3).children().children().eq(i).children().eq(0).text();
                        lahto.rata = rata;
                        lahto.pvm = data.children().eq(3).children().children().eq(i).children().eq(1).text();
                        lahto.lahto = parseInt(data.children().eq(3).children().children().eq(i).children().eq(2).text());
                        lahto.ratanro = parseInt(data.children().eq(3).children().children().eq(i).children().eq(3).text());
                        lahto.matka = parseInt(data.children().eq(3).children().children().eq(i).children().eq(4).text());
                        lahto.sijoitus = parseInt(data.children().eq(3).children().children().eq(i).children().eq(5).text());
                        lahto.kmaika = parseFloat(data.children().eq(3).children().children().eq(i).children().eq(7).text());
                        lahto.kerroin = parseFloat(data.children().eq(3).children().children().eq(i).children().eq(9).text());
                        lahto.palkkio = parseInt(data.children().eq(3).children().children().eq(i).children().eq(10).text());
                        lahto.valmentaja = data.children().eq(3).children().children().eq(i).children().eq(11).text();
                        lahto.ohjaaja = data.children().eq(3).children().children().eq(i).children().eq(12).text();
                        //console.log(rata + rata.length);
                        if (rata.length != 0){
                          heppa.lahdot.push(lahto);
                        }
                      }

                  })
                  //Tallennetaan heppa mongoon
                  heppa.save(function(err) {
                      if (err)
                          res.send(err);
                          console.log('Hevonen tallennettu:' + parseid);
                          parseid++;
                          Hevonen.count({}, function( err, plaa){
                          if (plaa === count + luku ){
                            parseid = 0;
                            console.log('Valmis objekteja tallennettu: ' + plaa);
                            res.json({"Hevosia lisättiin": req.params.loppu - req.params.alku,
                            "Hevosia kannassa": plaa});
                          }
                  });
                });
              };
            });
            alku++;
            heppaid++;
            setTimeout(loop, 50);
            //loop();
          }
          }());//while päättyy
        })
        };//parsi funktio
        });

return router;
});
