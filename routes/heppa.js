'use strict';

var mongoose = require('mongoose');
var Hevonen = require('../models/hevonen');
var express = require('express');
var passport = require('passport');
var loggedin = require("../helpers/loggedin");

module.exports = (function(router){

  router.get('/hevonen',function(req, res) {
              Hevonen.find(function(err, hevonen) {
                if (err){res.json({"error": "ei hakutuloksia"});
                }else{
                res.json(hevonen);
              }
          });
      });
  //tunnistautuminen ?usename=x&password=y
  router.get('/hevonen/:id', function(req, res) {
              Hevonen.findOne({"hevonenid": req.params.id}, function(err, hevonen) {
                  if (err){res.json({"error": "ei hakutuloksia"});
                  }else{
                  res.json(hevonen);
                }
              });
          });

  router.post('/hevonen/:id', passport.authenticate('local', { failureRedirect: '/' }), function(req, res) {
              Hevonen.findOne({"hevonenid": req.params.id}, function(err, hevonen) {
                    if (err){res.json({"error": "ei hakutuloksia"});
                    }else{
                      res.json(hevonen);
                    }
                  });
                });

  router.get('/hevonen/nimi/:id', function(req, res) {
                Hevonen.findOne({"nimi": req.params.id}, function(err, hevonen) {
                  if (err){res.json({"error": "ei hakutuloksia"});
                  }else{
                  res.json(hevonen);
                }
              });
          });

    router.delete('/hevonen/:id', function(req, res) {
          Hevonen.remove({
              hevonenid: req.params.id
          }, function(err, hevonen) {
              if (err)
                  res.send(err);

              res.json({ message: 'Poistettu' });
          });
      });

return router;
});
