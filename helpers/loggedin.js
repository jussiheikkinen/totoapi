var passport = require('passport');

module.exports = {
  isLoggedIn: function(req, res, next) {
    if (req.isAuthenticated())
        return next();
    //näytetään jos ei ole tunnistautunut
    res.json({"invalid": "acces"});
}
};
