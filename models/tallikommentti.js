var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Tallikommentti = new Schema({
    aika: Date,
    talli: String,
    kommentti: String
});

module.exports = mongoose.model('Tallikommentti', Tallikommentti);
