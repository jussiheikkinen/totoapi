var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Request = new Schema({
    aika: Date,
    ip: String
});

module.exports = mongoose.model('Request', Request);
