var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Viesti = new Schema({
    aika: Date,
    nimi: String,
    comment: String
});

module.exports = mongoose.model('Viesti', Viesti);
