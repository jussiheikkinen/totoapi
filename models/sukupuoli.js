var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Sukupuoli = new Schema({
    sukupuoli: String,
    startit: Number,
    voitot: Number,
    kakkoset: Number,
    kolmoset: Number
});

module.exports = mongoose.model('Sukupuoli', Sukupuoli);
