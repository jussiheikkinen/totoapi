var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Lahto = new Schema({
    rata: String,
    pvm: String,
    lahto: Number,
    ratanro: Number,
    matka: Number,
    sijoitus: Number,
    kmaika: Number,
    kerroin: Number,
    palkkio: Number,
    valmentaja: String,
    ohjaaja: String
});

module.exports = mongoose.model('Lahto', Lahto);
