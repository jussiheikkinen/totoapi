var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Vihje = new Schema({
    aika: Date,
    kilpailu: String,
    vihje: String
});

module.exports = mongoose.model('Vihje', Vihje);
