var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Hevonen = new Schema({
    hevonenid: Number,
    nimi: String,
    laji: String,
    sukupuoli: String,
    ika: Number,
    valmentaja: String,
    omistaja: String,

    ura: {
      startit: Number,
      voitot: Number,
      kakkoset: Number,
      kolmoset: Number,
      voittosumma: Number
    },
    ennatysajat: {
      ryhmalahto: Number,
      tasoitusajo: Number
    },
    lahdot: []
});

module.exports = mongoose.model('Hevonen', Hevonen);
