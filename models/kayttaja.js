var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Kayttaja = new Schema({
    local              : {
          username     : String,
          password     : String,
      }
});

Kayttaja.methods.validPassword = function(password) {
    if (password === this.local.password){
    return true;
  }
  return false;
};

module.exports = mongoose.model('Kayttaja', Kayttaja);
