angular.module('TilastotCtrl', []).controller('TilastotController', ['$scope', '$http', function($scope, $http) {

  $scope.haeHevonen = function(){

    if (!$scope.nimella){
        $http.get('/toto/hevonen/'+ $scope.haku).then(function(hepat) {
          $scope.hepat = hepat;
        });
        }
        else{
          $http.get('/toto/hevonen/nimi/'+ $scope.haku).then(function(hepat) {
          $scope.hepat = hepat;
          });
        }
    }
}]);
