angular.module('BlogiCtrl', []).controller('BlogiController', ['$scope', '$http', function($scope, $http) {

    $scope.lahetaViesti = function (){
      $http.post('/toto/viestit/uusi', {'nimi': $scope.nimi, 'viesti': $scope.viesti})
      .then(function() {
          $scope.viesti = "Lähetetty";
          $scope.init();
        });
    }

    $scope.init = function(){
      $http.get('/toto/viestit').then(function(viestit) {
          $scope.viestit = viestit.data;
        });
    }
}]);
