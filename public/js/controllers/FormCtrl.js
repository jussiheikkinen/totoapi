angular.module('FormCtrl', []).controller('FormController', ['$scope', '$http', function($scope, $http) {

$scope.vihjeet = [];
$scope.tallikommentit = [];

  $scope.lahetaVihje = function (){
    $http.post('/toto/lisaa/vihje', {'kilpailu': $scope.kilpailu, 'vihje': $scope.vihje})
    .then(function(res){
      $scope.vihjeet.push(res.data);
      });
  }

  $scope.lahetaTalliKommentti = function (){
    $http.post('/toto/lisaa/tallikommentti', {'talli': $scope.talli, 'kommentti': $scope.tallikommentti})
    .then(function(res){
      $scope.tallikommentit.push(res.data);
      });
  }

}]);
