'use strict';

angular.module('NavbarCtrl', ['ui.bootstrap'])
.controller('NavbarController', ['$scope', function($scope){
  $scope.isCollapsed = true;
}]);
