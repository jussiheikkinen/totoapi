angular.module('TotoService', []).factory('Toto', ['$http', function($http) {

    return {

        uusiviesti: function(nimi, viesti){
          return $http.post('/toto/viestit/uusi', {'nimi': nimi, 'viesti': viesti});
        },

        getviestit: function(){
          return $http.get('/toto/viestit');
        }
    }

}]);
