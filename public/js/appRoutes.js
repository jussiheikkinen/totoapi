 var app = angular.module('appRoutes', []);

 app.config(['$routeProvider', '$locationProvider', function($routeProvider, $locationProvider) {

    $routeProvider

        .when('/toto', {
            templateUrl: 'views/home.html',
            controller: 'MainController'
        })

        .when('/apicalls', {
            templateUrl: 'views/apicalls.html',
            controller: 'ApiController'
        })

        .when('/tilastot', {
            templateUrl: 'views/tilastot.html',
            controller: 'TilastotController'
        })

        .when('/keskustelu', {
            templateUrl: 'views/blogi.html',
            controller: 'BlogiController'
        })

        .when('/lisaa', {
          templateUrl: 'views/lisaa.html',
          controller: 'FormController'
        });

    $locationProvider.html5Mode(true);

}]);
